## User App
App has been made using hooks style along with redux & axios for state & api request.
https://react-table-3c63b.web.app/#/
### Packages used
* [Redux](https://react-redux.js.org/)
* [Axios](https://github.com/axios/axios)
* [React](https://reactjs.org/)


### Project Structure
* Actions
* Components
* Features
* Layout
* Reducers
* Store
* Types

1) Actions -: As named they are the actions that will change the state like fetching data, saving data & deleting data by user interaction
2) Components -: They are the fine grained components that will be used with features to complete it
3) Features -: Traditionally we use containers/ pages approach to build one but this feature approach seems quite good as it will be easier to extend & delete later on we build with feature not the view
4) Layout -: Handles the layout of different features
6) Reducers -: Listens the action triggered by user and make useful state for app to consume
7) Store -: Handles the initial state
*) Types -: All our constant used in actions & reducer will lie here.
   

### Deployment Strategy
Firebase has been used for the deployment and integrated with github action so on every master push it deploys to the production


### Things that could be done in future
* Use terraform to initialize multiple environement like currently there is only one
* Deploy on every tag release not the branch push
* Maintain a versioning strategy for release
* All our api req lies in actions it would be better if we could make some of domain or entities for large apps
* Fine grained components, currently there was no need of it as it would overkill
* UI tests