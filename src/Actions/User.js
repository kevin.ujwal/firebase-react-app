import axios from "axios";
import * as types from '../Types/User';

const listUserRequest = () => ({
    type: types.USER_LIST_REQUEST,
});

const listUserFailure = ({ error, responseStatus }) => ({
    type: types.USER_LIST_FAILURE,
    error,
    payload: {
        responseStatus,
    },
});

const listUserSuccess = (users ) => ({
    type: types.USER_LIST_SUCCESS,
    payload: {
        users,
    },
});

const API_URL = "https://firebase-api-llxhvovxlq-uc.a.run.app/";

const appRateRequest = () => ({
    type: types.APPRATE_LIST_REQUEST,
});

const appRateFailure = ({ error, responseStatus }) => ({
    type: types.APPRATE_LIST_FAILURE,
    error,
    payload: {
        responseStatus,
    },
});

const appRateSuccess = ( ) => ({
    type: types.APPRATE_LIST_SUCCESS,
});

export const listUserData = () => async (dispatch) => {

    dispatch(listUserRequest());
    try {
        const {data} = await axios.get(
            `${API_URL}users`,
        );
        return dispatch(listUserSuccess(data));
    } catch (errorResponse) {
        return dispatch(listUserFailure());
    }
};


export const rateApp = (rating,appName) => async (dispatch) => {

    dispatch(appRateRequest());
    try {
         await axios.post(
            `${API_URL}/rate/app`,
            {
                rating : rating,
                name: appName
            }
        );
        return dispatch(appRateSuccess());
    } catch (errorResponse) {
        return dispatch(appRateFailure());
    }
};