import * as types from './../Types/User'
import initialState from "../Store/initia-state";

export function users(state = initialState.app, action) {
    switch (action.type) {
        case types.USER_LIST_SUCCESS:
            return {
                ...state,
                userListLoaded: true,
                users:action.payload.users.data
            };
        default:
            return state
    }
}