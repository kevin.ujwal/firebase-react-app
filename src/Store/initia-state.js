
const state = {
    app: {
        userListLoaded: false,
        error: '',
        message: '',
        users: []
    },
};

export default state;
