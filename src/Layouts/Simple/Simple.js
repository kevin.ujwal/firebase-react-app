import React from 'react';
import PropTypes from 'prop-types';

export default function Simple({ children }) {
  return (
    <div className="container">
        <header>Fine app</header>
      <main className="container">{children}</main>
    </div>
  );
}

Simple.propTypes = {
  children: PropTypes.node.isRequired,
};
