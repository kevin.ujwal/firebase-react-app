/* eslint-env jest */
/* eslint-disable padded-blocks, no-unused-expressions */

import React from 'react';
import renderer from 'react-test-renderer';
import SimpleLayout from './Simple';

describe('SimpleLayout', () => {
  test('renders children correctly', () => {
    const wrapper = renderer
      .create(
        <SimpleLayout>
          <div className="child" />
        </SimpleLayout>,
      )
      .toJSON();

    expect(wrapper).toMatchSnapshot();
  });
});
