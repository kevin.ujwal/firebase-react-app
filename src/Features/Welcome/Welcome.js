import {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {listUserData, rateApp} from "../../Actions/User";
import './welcome.css'
import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'
import Header from "../../Components/Header";
function Welcome() {
    const dispatch = useDispatch();
    const {users} = useSelector((state => state.users));

    const rate =  ({rating},appName) => {
        dispatch(rateApp(rating,appName))
    }

    useEffect( () => {
        dispatch(listUserData());

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div>
            <Header/>
            <table className="styled-table">
                <thead>
                <tr>
                    <th>User name</th>
                    <th>Account id</th>
                    <th>App Name</th>
                    <th>App Rating</th>
                </tr>
                </thead>
                <tbody>

                {users.map((user) => {
                    return <tr key={user.id}>
                        <td>{user.name}</td>
                        <td>{user.account}</td>
                        <td>{Object.keys(user.apps).map((id) => {
                            return <span key={id}>{user.apps[id].title}</span>
                        })}</td>

                        <td>{Object.keys(user.apps).map((id) => {
                            return <Rater key={id} total={5} rating={user.apps[id].rating ?? 0} onRate={(e) => {rate(e,id)}} />
                        })}</td>


                    </tr>
                })}


                </tbody>
            </table>
        </div>
    );
}

export default Welcome;


