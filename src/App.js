import './App.css';
import { Provider } from 'react-redux';
import {HashRouter as Router, Route, Switch} from "react-router-dom";
import { createHashHistory } from 'history';
import Welcome from "./Features/Welcome/Welcome";
import {applyMiddleware, createStore} from "redux";
import rootReducer from "./Reducers";
import thunk from "redux-thunk";

function App({Component,pageProps}) {


    const store = createStore(rootReducer,applyMiddleware(thunk))
  return (
      <Provider store={store}>
          <Router history={createHashHistory()}>
              <Switch>
                  <Route exact path="/" component={Welcome} />
              </Switch>
          </Router>
      </Provider>
  );
}

export default App;
